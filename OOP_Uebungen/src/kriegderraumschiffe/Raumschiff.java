package kriegderraumschiffe;

import java.util.ArrayList;
import java.lang.Math;
import java.util.Random;


/**
 * Raumschiff (Spaceship)
 * @author Justin Riedel
 * @version 1.0
 * 18.03.2021
 *
 */

public class Raumschiff {
	
	private String name;
	private int powerSupply;
	private ArrayList<String> logbookEntries = new ArrayList<String>();
	
	private int photonTorpedoCount;
	private int androidCount;
	private int hitCount;
	
	private int shieldStatus;
	private int lifesupportStatus;
	private int shellStatus;

	private static ArrayList<String> broadcastCommunicator = new ArrayList<String>();
	private ArrayList<Ladung> load = new ArrayList<Ladung>();
	
	
	public Raumschiff() {}	
	
	/**
	 * Creates the instance of spaceship class.
	 * @param name - Name of Spaceship.
	 * @param powerSupply - Spaceship Power-Supply in percent.
	 * @param lifesupportStatus - Spaceship life support system status in percent.
	 * @param shieldStatus - Spaceship shield status in percent.
	 * @param shellStatus - Spaceshipp shell status in percent.
	 * @param anzahlPhotonentorpedoes - Count of photon torpedos in spaceship.
	 * @param androidCount - Count of androids in spaceship.
	 */
	public Raumschiff(String name, int powerSupply, int lifesupportStatus, int shieldStatus, int shellStatus, int photonTorpedoCount, int androidCount) {
		setName(name);
		setPowerSupply(powerSupply);
		setLifesupportStatus(lifesupportStatus);
		setShieldStatus(shieldStatus);
		setShellStatus(shellStatus);
		setPhotonTorpedoCount(photonTorpedoCount);
		setAndroidCount(androidCount);
	}
	
	/**
	 * Creates an overview of the status of the spaceship and outputs it on the console.
	 * 
	 */
	public void getSpaceshipStatus() {
		
		// Output all relevant spaceship information.
		System.out.printf("Raumschiffstatus f�r das Raumschiff: %s\n", this.name);
		System.out.printf("Energieversorgung: %d\n", this.powerSupply);
		System.out.printf("Lebenserhaltungssystem: %d\n", this.lifesupportStatus);
		System.out.printf("Schutzschild: %d\n", this.shieldStatus);
		System.out.printf("H�lle: %d\n", this.shellStatus);
		System.out.println();
		
		// Output load by iterating over load ArrayList
		System.out.printf("Ladungen vom Raumschiff: %s\n", this.name);
		for(int i = 0; i < load.size(); i++) {
			// output formatted list of load type and count
			System.out.printf("%s: %d", this.load.get(i).getType(), this.load.get(i).getCount());
			System.out.println();
		}
		System.out.println();
	}
		
	/**
	 * Returns the spaceships name.
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the spaceships name.
	 * @param name - name of spaceship
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Return the spaceships power supply in percent.
	 * @return powerSupply 
	 */
	public int getPowerSupply() {
		return powerSupply;
	}
	
	/**
	 * Sets the spaceships power supply in percent.
	 * @param powerSupply - value of power supply (between 1-100)
	 */
	public void setPowerSupply(int powerSupply) {
		this.powerSupply = powerSupply;
	}
	
	/**
	 * Return all the logbook entries of the spaceship.
	 * @return logbookEntries - All logbook entries in String ArrayList format.
	 */
	public ArrayList<String> getLogbookEntries() {
		return logbookEntries;
	}
	
	/**
	 * Sets the logbook entries, while overwriting existing ones. 
	 * @param logbookEntries - String ArrayList of logbook entries 
	 */
	public void setLogbookEntries(ArrayList<String> logbookEntries) {
		this.logbookEntries = logbookEntries;
	}
	
	/**
	 * Returns the spaceships hit count.
	 * @return hitCount 
	 */
	public int getHitCount() {
		return hitCount;
	}
	
	/**
	 * Sets the spaceships hit count.
	 * @param hitCount - Number of hits.
	 */
	public void setHitCount(int hitCount) {
		this.hitCount = hitCount;
	}
	
	/**
	 * Returns the status of the spacehips life-support system
	 * @return Life-support system status in percent (1-100 range).
	 */
	public int getLifesupportStatus() {
		return lifesupportStatus;
	}
	
	/**
	 * Sets the status of the spaceships life-support system.
	 * @param lifesupportStatus - Life-support system status in percent (1-100 range).
	 */
	public void setLifesupportStatus(int lifesupportStatus) {
		this.lifesupportStatus = lifesupportStatus;
	}
	
	/**
	 * Returns the status of the spaceships shield.
	 * @return Shield status in percent (1-100 range).
	 */
	public int getShieldStatus() {
		return shieldStatus;
	}
	
	/**
	 * Sets the status of the spaceships shield.
	 * @param shieldStatus - Shield status in percent (1-100 range).
	 */
	public void setShieldStatus(int shieldStatus) {
		this.shieldStatus = shieldStatus;
	}
	
	/**
	 * Return the current spaceships shell status.
	 * @return  Shell status in percent (1-100 range).
	 */
	public int getShellStatus() {
		return shellStatus;
	}
	
	/**
	 * Sets the spaceships shell status.
	 * @param shellStatus - Shell status in percent (1-100 range).
	 */
	public void setShellStatus(int shellStatus) {
		this.shellStatus = shellStatus;
	}
	
	/**
	 * Returns the  the number of photonTorpedoCount on the spaceship.
	 * @return Count of photon torpedoes.
	 */
	public int getPhotonTorpedoCount() {
		return photonTorpedoCount;
	}
	
	/**
	 * Sets the  the number of photon torpedoes on the spaceship.
	 * @param photonTorpedoCount - Count of photon torpedoes.
	 */
	public void setPhotonTorpedoCount(int photonTorpedoCount) {
		this.photonTorpedoCount = photonTorpedoCount;
	}
	
	/**
	 * Returns the number of androids on the spaceship.
	 * @return Count of androids.
	 */
	public int getAndroidCount() {
		return androidCount;
	}
	
	/**
	 * Sets the number of androids on the spaceship.
	 * @param androidCount - Counts of androids.
	 */
	public void setAndroidCount(int androidCount) {
		this.androidCount = androidCount;
	}
	
	/**
	 * Returns the spaceships broadcast communicator.
	 * @return Broadcast Communicator
	 */
	public ArrayList<String> getBroadcastCommunicator() {
		return broadcastCommunicator;
	}
	
	/**
	 * Sets the spaceships broadcast communicator.
	 * @param broadcastCommunicator 
	 */
	public void setBroadcastCommunicator(ArrayList<String> broadcastCommunicator) {
		Raumschiff.broadcastCommunicator = broadcastCommunicator;
	}
	
	/**
	 * Adds a load object to the spaceships load array.
	 * @param load - Instance of Ladung
	 */
	public void addLoad(Ladung load) {
		this.load.add(load);
	}
	
	/**
	 * Returns the spaceships load array list.
	 * @return ArrayList of Load Objects.
	 */
	public ArrayList<Ladung> getLoad(){
		return this.load;
	}
	
	/**
	 * Cleans up the spaceships loads by removing empty loads.
	 *
	 */
	public void cleanupLoad() {
		for(int i = 0; i < this.load.size(); i++) {
			// check if the load is empty, in case remove it from load
			if(this.load.get(i).getCount() == 0) {
				this.load.remove(i);
			}
		}
	}

	/**
	 * Send a repair order to the androids of the spaceship
	 * @param shield - Repair shield (true/false).
	 * @param shell - Repair shell (true/false).
	 * @param lifesupport - Repair life-support (true/false).
	 * @param androidCount - Count of androids.
	 */
	public void sendRepairOrder(boolean shield, boolean shell, boolean lifesupport, int androidCount) {
		Random random = new Random();
		int randomNo = random.nextInt(101);
		int componentCount = 0;
		
		if(androidCount > this.androidCount) {
			androidCount = this.androidCount;
		}
		// sum up the component count
		if(shield) componentCount++;
		if(shell) componentCount++;
		if(lifesupport) componentCount++;
		
		int calcResult = 0;
		// multiplication of the random number by the amount of the number of androids inserted 
		// afterwards  divided by the count of components that should be repaired
		calcResult = (randomNo * Math.abs(androidCount)) / Math.abs(componentCount);
			
		// add the result to the components that should be repaired
		if(shield) this.shieldStatus += calcResult;
		if(shell) this.shellStatus += calcResult;
		if(lifesupport) this.lifesupportStatus += calcResult;
		
	} 
	/**
	 * Shoots photon torpedoes at another spaceship.
	 * @param shotCount - Number of photon torpedos, which are to be shot down
	 * @param spaceship - The target spaceship.
	 */
	public void shootPhotonTorpedos(int shotCount, Raumschiff spaceship) {
		boolean photonTorpedoesFound = false;
		int i = 0;
	
		// search for photon torpedoes in load by iterating over loads ArrayList
		while(photonTorpedoesFound == false && i < this.load.size()){
			int loadCount = this.load.get(i).getCount(); 
			String loadType = this.load.get(i).getType();
			if(loadType == "Photonentorpedos" && loadCount > 0) {
				photonTorpedoCount += this.load.get(i).getCount();
				this.load.get(i).setCount(0);
				photonTorpedoesFound = true;
			}
			else {
				i++;
			}
		}
		// if there is no load of photon torpedoes on the ship, a note is output in console. 
		// In addition a message "-=*Click*=-" is send.
		if(this.photonTorpedoCount <= 0) {
			System.out.println("Keine Photonentorpedoes vorhanden");
			while(shotCount > 0) {
				sendMessage("-=*Click*=-");
				shotCount = shotCount - 1;
			}	
		}
		else if (shotCount > this.photonTorpedoCount) {
			System.out.printf("%d Photonentorpedos eingesetzt\n", this.photonTorpedoCount);
			while(this.photonTorpedoCount > 0) {
				registerAttack(spaceship);
				this.photonTorpedoCount = this.photonTorpedoCount - 1;
			}
			
		}
		else {
			setPhotonTorpedoCount(this.photonTorpedoCount - shotCount);
			System.out.printf("%d Photonentorpedos eingesetzt\n", shotCount);
			while(shotCount > 0) {
				registerAttack(spaceship);
				shotCount = shotCount - 1;
				this.photonTorpedoCount = this.photonTorpedoCount - 1; 
			}
			
		}
	}
	
	/**
	 * Register a attack on given spaceship.
	 * @param spaceship - Target Spaceship
	 */
	private void registerAttack(Raumschiff spaceship) {
		System.out.printf("%s wurde getroffen\n", spaceship.name);
		if(spaceship.shieldStatus > 0) {
			spaceship.shieldStatus = spaceship.shieldStatus - 50;
			if(spaceship.shieldStatus <= 0) {
				spaceship.shellStatus = spaceship.shellStatus - 50;
				spaceship.powerSupply = spaceship.powerSupply - 50;
					if(spaceship.shellStatus <= 0) {
						spaceship.lifesupportStatus = 0;
						sendMessage("Lebenserhaltungssysteme wurden zerst�rt.");
					}	
			}
		}
	}

	/**
	 * Shoots a phaser cannon on another spaceship.
	 * @param spaceship - Target Spaceship
	 */
	public void shootPhaserCannon(Raumschiff spaceship) {
		if(this.powerSupply < 50) {
			sendMessage("-=*Click*=-");
		}
		else {
			powerSupply = this.powerSupply - 50;
			sendMessage("Phaserkanone abgeschossen");
			registerAttack(spaceship);
		}
	}
	
	
	/**
	 * Sends a message, adds it to the broadcast communicator and outputs it in the console.
	 * @param message - message content
	 */
	public void sendMessage(String message) {
		Raumschiff.broadcastCommunicator.add(message);
		this.logbookEntries.add(message);
		System.out.println(message);
	}
	
}
