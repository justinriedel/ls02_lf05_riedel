package kriegderraumschiffe;

public class Ladung {
	
	private String type;
	private int count;
	
	public Ladung() {
		
	}
	
	public Ladung(String type, int count) {
		setType(type);
		setCount(count);
	}
	
	public void getLoad(String type, int count) {
		System.out.printf("Ladungsart: %s", type);
		System.out.printf("Anzahl: %d", count);
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}

	
	
	
}
