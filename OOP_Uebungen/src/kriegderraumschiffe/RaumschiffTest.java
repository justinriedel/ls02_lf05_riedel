package kriegderraumschiffe;


/**
 * RaumschiffTest 
 * @author Justin Riedel
 * @version 1.0
 * 18.03.2021
 *
 */
public class RaumschiffTest {

	/* 
	 * Instancing spaceship and load objects, performing serveral actions.
	 * @param args  
	 */
	public static void main(String[] args) {
		
		// instancing spaceship objects
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 1, 2);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 100, 80, 50, 0, 5);
		
		// instancing load objects
		Ladung load1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung load2 = new Ladung("Bat'leth Kligonen Schwert", 200);
		Ladung load3 = new Ladung("Borg-Schrott", 5);
		Ladung load4 = new Ladung("Rote Materie", 2);
		Ladung load5 = new Ladung("Plasma-Waffe", 50);
		Ladung load6 = new Ladung("Forschungssonde", 35);
		Ladung load7 = new Ladung("Photonentorpedos", 3);
		
		// add instanced loads to spaceships
		klingonen.addLoad(load1);
		klingonen.addLoad(load2);
		romulaner.addLoad(load3);
		romulaner.addLoad(load4);
		romulaner.addLoad(load5);
		vulkanier.addLoad(load6);
		vulkanier.addLoad(load7);
		
		klingonen.shootPhotonTorpedos(1, romulaner);
		romulaner.shootPhaserCannon(klingonen);
		vulkanier.sendMessage("Gewalt ist nicht logisch.");
		klingonen.getSpaceshipStatus();
		vulkanier.sendRepairOrder(true, true, false, 5);
		vulkanier.setPhotonTorpedoCount(load7.getCount() + vulkanier.getPhotonTorpedoCount());
		load7.setCount(0);
		vulkanier.cleanupLoad();
		klingonen.shootPhotonTorpedos(2, romulaner);
		klingonen.getSpaceshipStatus();
		romulaner.getSpaceshipStatus();
		vulkanier.getSpaceshipStatus();
	}

}
