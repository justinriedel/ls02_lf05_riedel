package rechteck;

public class Rechteck {

	    private double width;
	    private double height;

	    public Rechteck(double width, double height) {
	        this.width = width;
	        this.height = height;
	    }

		public double getWidth() {
			return this.width;
		}
	    
		public double getHeight() {
			return this.height;
		}
		
		public void setWidth(double width) {
			if(width > 0) {
				this.width = width;
			}
		}
		
		public void setHeight(double height) {
			if(height > 0) {
				this.width = height;
			}
		}
	    public double getArea() {
	        return width * height;
	    }

	    public double getCircumference() {
	        return 2*width+2*height;
	    }
  
    
	
}
