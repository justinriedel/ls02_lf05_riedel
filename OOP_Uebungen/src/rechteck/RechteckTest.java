package rechteck;

public class RechteckTest {

	public static void main(String[] args) {

		Rechteck rechteck = new Rechteck(5, 10);
		
		System.out.printf("Rechteck (H�he: %.2f, Breite: %.2f) \n \n", rechteck.getHeight(), rechteck.getWidth());
		
		System.out.println("Fl�cheninhalt: "+ rechteck.getArea());
		System.out.println("Umfang: "+rechteck.getCircumference());
				
	}

}
